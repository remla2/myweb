package remla.myweb.ctrls;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mylib.RemlaUtil;

@Controller
public class HelloWorldCtrl {

	@GetMapping("/")
	@ResponseBody
	public String index() {
		return String.format("Hello world from %s! (util version: %s)", RemlaUtil.getHostName(), RemlaUtil.getUtilVersion());
	}
}
